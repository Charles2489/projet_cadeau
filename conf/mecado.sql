-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 10 Novembre 2017 à 21:03
-- Version du serveur :  5.1.73
-- Version de PHP :  7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `razojaba1u`
--

-- --------------------------------------------------------

--
-- Structure de la table `acheteur`
--

CREATE TABLE IF NOT EXISTS `acheteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `participation` float DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `id_item` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_acheteur_id_item` (`id_item`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Contenu de la table `acheteur`
--

INSERT INTO `acheteur` (`id`, `nom`, `prenom`, `participation`, `message`, `id_item`) VALUES
(1, 'LAURA', 'RODRIGUEZ', 12.5, 'Message de félicitations de LAURA', 32),
(2, 'ROBERTO', 'BARRERA', NULL, 'Message de félicitations de ROBERTO', 35),
(3, 'ESTELA', 'MONTENEGRO', 13.5, 'Message de félicitations de ESTELA', 36),
(4, 'MARIA', 'MENDEZ', NULL, 'Message de félicitations de MARIA', 39),
(5, 'VICTOR', 'FUENTES', 16.5, 'Message de félicitations de VICTOR', 40),
(6, NULL, NULL, NULL, NULL, 40),
(7, 'GOOD', 'NIGHT', NULL, 'This is Halloween', 40),
(8, 'RISE YOUR GLASS', 'RISE YOUR GLASS', NULL, 'RISE YOUR GLASS', 41),
(9, 'DARK ', 'DARK', NULL, 'DARK', 41),
(10, 'BOB', 'BOB', NULL, 'BOB', 40),
(11, 'test', 'ujEAN', NULL, 'SDQSQDDSQ', 41),
(12, 'H', 'H', NULL, 'H', 41),
(13, 'I', 'I', NULL, 'I', 41),
(14, '5', '5', NULL, '5', 40),
(15, 'DOC', 'DOC', NULL, 'DOC', 40),
(16, 'COOL', 'COOL', NULL, 'COOL', 40),
(17, NULL, NULL, NULL, NULL, 40),
(18, NULL, NULL, NULL, NULL, 40),
(19, NULL, NULL, NULL, NULL, 40),
(20, NULL, NULL, NULL, NULL, 40),
(21, 'DSF', 'QSDF', NULL, 'FQSDF', 40),
(22, NULL, NULL, NULL, NULL, 40),
(23, NULL, NULL, NULL, NULL, 40),
(24, NULL, NULL, NULL, NULL, 40),
(25, NULL, NULL, NULL, NULL, 40),
(26, NULL, NULL, NULL, NULL, 40),
(27, NULL, NULL, NULL, NULL, 40),
(28, NULL, NULL, NULL, NULL, 40),
(29, NULL, NULL, NULL, NULL, 40),
(30, NULL, NULL, NULL, NULL, 40);

-- --------------------------------------------------------

--
-- Structure de la table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tarif` float DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `groupe` tinyint(1) DEFAULT NULL,
  `cagnotte` tinyint(1) DEFAULT NULL,
  `item_parent` int(11) DEFAULT NULL,
  `id_liste` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_item_id_liste` (`id_liste`),
  KEY `FK_item_id_item_parent` (`item_parent`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Contenu de la table `item`
--

INSERT INTO `item` (`id`, `nom`, `description`, `tarif`, `url`, `image`, `groupe`, `cagnotte`, `item_parent`, `id_liste`) VALUES
(32, 'iPod', 'Description iPod', 50.5, 'iPod URL', 'iPod IMAGE', 0, 1, NULL, 20),
(33, 'iPhone', 'Description iPhone', 60.6, 'iPhone URL', 'iPhone IMAGE', 0, 0, NULL, 20),
(34, 'iMac', 'Description iMac', 70.7, 'iMac URL', 'iMac IMAGE', 0, 1, NULL, 21),
(35, 'MacBook', 'Description MacBook', 80.8, 'MacBook URL', 'MacBook IMAGE', 0, 0, NULL, 21),
(36, 'iWatch', 'Description iWatch', 90.9, 'iWatch URL', 'iWatch IMAGE', 0, 1, NULL, 22),
(37, 'Samsung Galaxy Tab', 'Description Samsung Galaxy Tab', 40.4, 'Samsung Galaxy Tab URL', 'Samsung Galaxy Tab IMAGE', 0, 0, NULL, 22),
(38, 'Launchpad', 'Description Launchpad', 30.3, 'Launchpad URL', 'Launchpad IMAGE', 0, 1, NULL, 23),
(39, 'Maison', 'Description Maison', 200.2, 'Maison URL', 'Maison IMAGE', 0, 0, NULL, 23),
(40, 'HP 360', 'Description HP 360', 500.5, 'url', 'https://images.duckduckgo.com/iu/?u=http%3A%2F%2Fssl-product-images.www8-hp.com%2Fdigmedialib%2Fprodimg%2Flowres%2Fc05218075.png&f=1', 0, 1, NULL, 24),
(41, 'HP Envy', 'Description HP Envy', 600.6, 'HP Envy URL', 'HP Envy IMAGE', 0, 0, NULL, 24);

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE IF NOT EXISTS `liste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_limite` date DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `pour_moi` tinyint(1) DEFAULT NULL,
  `prenomDestinataire` varchar(25) DEFAULT NULL,
  `id_utilisateur` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_liste_id_utilisateur` (`id_utilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Contenu de la table `liste`
--

INSERT INTO `liste` (`id`, `nom`, `description`, `date_limite`, `url`, `pour_moi`, `prenomDestinataire`, `id_utilisateur`, `updated_at`, `created_at`) VALUES
(20, 'Mon anniversaire', 'Description Anniversaire\r\nDon''t stomeeee noooow', '2017-11-30', NULL, 1, NULL, 5, NULL, '2017-11-10'),
(21, 'Mariage', 'Description Mariage\r\nI''ll be your light, your match, your burning sun,', '2017-11-29', NULL, 1, NULL, 6, '2017-11-10', '2017-11-10'),
(22, 'Graduation', 'Description Graduation', '2017-11-28', NULL, 0, 'ALICE', 7, '2017-11-10', '2017-11-10'),
(23, 'Anniversaire de mariage', 'Description Anniversaire de mariage', '2017-11-27', NULL, 1, NULL, 8, NULL, '2017-11-10'),
(24, 'Anniversaire', 'Description Anniversaire', '2017-11-26', NULL, 0, 'ROBERTO', 9, NULL, '2017-11-10'),
(25, 'Mariage', 'Je me marie avec Maria, j''attends de vous plein de cadeau.', '2017-12-15', NULL, NULL, '', 9, NULL, NULL);

--
-- Déclencheurs `liste`
--
DROP TRIGGER IF EXISTS `new_list_sha`;
DELIMITER //
CREATE TRIGGER `new_list_sha` AFTER INSERT ON `liste`
 FOR EACH ROW INSERT INTO url(id_liste, liste_url) VALUES (NEW.id, SHA1(NEW.id))
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `auteur` varchar(25) DEFAULT NULL,
  `id_liste` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_message_id_liste` (`id_liste`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `message`
--

INSERT INTO `message` (`id`, `message`, `auteur`, `id_liste`) VALUES
(1, 'Message 01.', NULL, 20),
(2, 'Message 02.', NULL, 21),
(3, 'Message 03.', NULL, 22),
(4, 'Message 04.', NULL, 23),
(5, 'Message 05.', NULL, 24);

-- --------------------------------------------------------

--
-- Structure de la table `url`
--

CREATE TABLE IF NOT EXISTS `url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_liste` int(11) NOT NULL,
  `liste_url` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_url_list` (`id_liste`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Contenu de la table `url`
--

INSERT INTO `url` (`id`, `id_liste`, `liste_url`) VALUES
(15, 20, '91032ad7bbcb6cf72875e8e8207dcfba80173f7c'),
(16, 21, '472b07b9fcf2c2451e8781e944bf5f77cd8457c8'),
(17, 22, '12c6fc06c99a462375eeb3f43dfd832b08ca9e17'),
(18, 23, 'd435a6cdd786300dff204ee7c2ef942d3e9034e2'),
(19, 24, '4d134bc072212ace2df385dae143139da74ec0ef'),
(20, 25, 'f6e1126cedebf23e1463aee73f9df08783640400');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(25) DEFAULT NULL,
  `prenom` varchar(25) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `mdp` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `nom`, `prenom`, `mail`, `mdp`) VALUES
(5, 'MERMET', 'Amelie', 'amelie@gmail.com', '7da4a522b5a802144fdedbcd30d346978b3e07dc'),
(6, 'MESCOFF', 'Isabelle', 'isabelle@gmail.com', '7da4a522b5a802144fdedbcd30d346978b3e07dc'),
(7, 'BRIAND', 'Odette', 'odette@gmail.com', '7da4a522b5a802144fdedbcd30d346978b3e07dc'),
(8, 'LEDUC', 'Adrien', 'adrien@gmail.com', '7da4a522b5a802144fdedbcd30d346978b3e07dc'),
(9, 'MOREAU', 'Pierre', 'pierre@gmail.com', 'remy123'),
(10, 'LAFAYETE', 'Remi', 'remy@gmail.com', '7da4a522b5a802144fdedbcd30d346978b3e07dc');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `acheteur`
--
ALTER TABLE `acheteur`
  ADD CONSTRAINT `FK_acheteur_id_item` FOREIGN KEY (`id_item`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `FK_item_id_item_parent` FOREIGN KEY (`item_parent`) REFERENCES `item` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_item_id_liste` FOREIGN KEY (`id_liste`) REFERENCES `liste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `liste`
--
ALTER TABLE `liste`
  ADD CONSTRAINT `FK_liste_id_utilisateur` FOREIGN KEY (`id_utilisateur`) REFERENCES `utilisateur` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_message_id_liste` FOREIGN KEY (`id_liste`) REFERENCES `liste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `url`
--
ALTER TABLE `url`
  ADD CONSTRAINT `FK_url_list` FOREIGN KEY (`id_liste`) REFERENCES `liste` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
