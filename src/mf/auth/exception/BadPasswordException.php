<?php

namespace mf\auth\exception;

class BadPasswordException extends AuthentificationException { }