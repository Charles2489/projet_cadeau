<?php

namespace mf\auth\exception;

class UnknownUserException extends AuthentificationException { }