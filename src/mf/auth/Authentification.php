<?php

namespace mf\auth;

class Authentification extends AbstractAuthentification {



    /* constructeur */
    public function __construct(){
        if(isset($_SESSION['user_login'])){
            $this->user_login = $_SESSION['user_login'];
            $this->access_level = $_SESSION['access_level'];
            $this->logged_in = true;
        }
        else{
            $this->user_login = null;
            $this->access_level = null;
            $this->logged_in = false;

        }
    }

    
    

    public function updateSession($username, $level){
        $this->user_login = $username;
        $this->access_level = 1;
        $_SESSION['user_login'] = $username;
        $_SESSION['access_level'] = $level;
        //$_SESSION['test'] = 'bonjour';
        $this->logged_in = true;
    }

    public function logout(){
        $this->user_login = null;
        $this->access_level = ACCESS_LEVEL_NONE;
        $this->logged_in = false;
    }

    public function checkAccessRight($requested){
        if($requested > $this->access_level){
            return false;
        }
        else{
            return true;
        }
    }

    protected function hashPassword($password){
        return password_hash($password);
    }

    protected function verifyPassword($password, $hash){
        //return password_verify($password, $hash);
        return $password == $hash;
    }

}
