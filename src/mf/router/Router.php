<?php

namespace mf\router;

class Router extends AbstractRouter{

	

	public function __construct(){
        parent::__construct();
    }

	public function addRoute($name, $url, $ctrl, $mth){

		self::$routes[$name] = array($ctrl, $mth);
		self::$routes[$url] = array($ctrl, $mth);

	}

	public function run(){
		$url = $this->http_req->path_info;
		if(isset(self::$routes[$url])){
			$ctrl = self::$routes[$url][0];
			$mth = self::$routes[$url][1];
			$c = new $ctrl();
			$c->$mth();
		
		}
		else{
			$ctrl = self::$routes['default'][0];
			$mth = self::$routes['default'][1];
			$c = new $ctrl();
			$c->$mth();
			//Executer la méthode de la route par défaut
		}
	}

}