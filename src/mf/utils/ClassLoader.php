<?php

	class ClassLoader{
	
	private $prefix;

	public function __construct($prefix){
		$this->prefix = $prefix;
	}

	public function loadClass($name){
		$name = $this->prefix."/".$name.'.php';
		$c = str_replace('\\', DIRECTORY_SEPARATOR, $name);
		if(file_exists($c)){
			require $c;	
		}

		//return $c;
	}

	public function register(){
		spl_autoload_register(array($this, 'loadClass'));
		
	}
}
