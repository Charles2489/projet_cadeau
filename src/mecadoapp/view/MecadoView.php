<?php

namespace mecadoapp\view;

class MecadoView extends \mf\view\AbstractView {

    /* Constructeur 
    *
    * Appelle le constructeur de la classe \mf\view\AbstractView
    */
public function __construct( $data ){
        parent::__construct($data);
    }


private function renderHeader(){
        $app_root = $this->app_root;
        if(isset($_SESSION['user_login'])) {
            $html =<<<EOT
            <h1>BIENVENUE SUR MECADO.NET</h1>
            <a href='${app_root}/main.php/logout/' class='btn btnDanger btnConnexion upperCase'>Deconnexion</a>
EOT;

        
}
else{
   $html =<<<EOT
   <h1>BIENVENUE SUR MECADO.NET</h1>
   <a href='${app_root}/main.php/signUp/' class='btn btnSucces btnConnexion upperCase'>Inscription</a>
EOT;
  
     }

    return $html;
 }

private function renderViewListe(){
    $lien = $this->app_root;
    $prenomUser = $_SESSION['user_login'];
    $html =<<<EOT

    <h2>Voici toute vos listes :</h2>
    <div class="grilleListe">
    
EOT;

    foreach ($this->data as $v) {
        $id=  $v->id;
        $nom = $v->nom;
        $desc = $v->description;
        $date_limite = $v->date_limite ;
        $url_objet=\mecadoapp\model\Url::where('id_liste', '=', $id)->first();
        // $url = 'http://localhost/razojaba1u/atelier/projet_cadeau/main.php/view-url-list?liste_url=' . $url_objet->liste_url;
        $url = $lien.'/main.php/view-url-list/?liste_url=' . $url_objet->liste_url;
        $pour_moi = $v->pour_moi;
        if ($v->prenomDestinataire != Null) {
            $prenomDestinataire = $v->id;
        }

        $html .=<<<EOT

        <article>
            <div>
                <a class="list left" href="${lien}/main.php/delete-list/?id=${id}"><img class="suppListe" src="../../html/img/error.png" name="supp" /></a>
            </div>

            <a class="list" href="${lien}/main.php/list-items/?liste_id=${id}">
                <div>
                    <h3 class="upperCase">${nom}</h3>
                    <p class="bold">${desc}</p>
                    <p> <span class="bold">Date d'expiration del'événement : </span>${date_limite}</p>
                </div>
            </a>
            <p><a href="${url}"><span class="upperCase bold">Acceder à la liste en tant qu'invité</span></a></p>
            <div class="addMsg">
                <a href="${lien}/main.php/add-list-message/?liste_id=${id}"><img src="../../html/img/chat.png" name="addMsg" /></a>
                <a class="list" href="${lien}/main.php/edit-list/?id=${id}"><img src="../../html/img/edit.png" name="addMsg" /></a>
            </div>
        </article>

       

EOT;
        }

        $html .=<<<EOT
            <article>
                <a class="list" href='${lien}/main.php/add-list/'>
                    <div>
                        <h3 class="upperCase">Ajouter une liste</h3>
                        <img class="basicImg" src="../../html/img/add.png" name="addList" />
                    </div>
                </a>
            </article>
        </div>

        
EOT;
        return ($html);
    }

private function renderViewListItem(){
    $messages = \mecadoapp\model\Message::where('id_liste', '=', $_SESSION["liste_id"])->get();
    $message="";
    foreach ($messages as $m) {
        $message = $m->message;
    }
    $lien = $this->app_root;


    $html =<<<EOT

    <div class="msgNotif justify">
    <h3>Message : </h3>
    <p>${message}</p>
    </div>


    <h2>Liste : </h2>
    <div class="grilleListe">
EOT;
    
  

    foreach ($this->data as $v) {
        $nom = $v->nom;
        if ($v->image ==null) {
            $image = "$lien/html/img/default.png";
        }
        else {
            $image = $v->image;
        }
        $desc = $v->description;
        $date_limite = $v->date_limite;
        $url = $v->url;
        $tarif=$v->tarif;
        $id = $v->id;
        if ($v->prenomDestinataire != Null) {
            $prenomDestinataire = $v->id;
        }

        $html .=<<<EOT

        
            <article>
            <a class="list left" href="${lien}/main.php/delete-item/?id=${id}"><img class="suppListe" src="../../html/img/error.png" name="supp" /></a>
                <a class="list" href="">
                    <div>
                        <h3 class="upperCase">${nom}</h3>
                        <div class="divImg">
                        <img class="basicImg" src="${image}" name="image">
                        </div>
                        <p class="bold">${desc}</p>
                        <p>${tarif} €</p>
                        
                    </div>
                </a>
                <a href="${url}" class="bold">URL : ${url}</a>
                <div class="addMsg">
                <a class="list" href="${lien}/main.php/edit-item/?id=${id}"><img src="../../html/img/edit.png" name="addMsg" /></a>
                </div>
            </article>
        
    
        
EOT;
        }
        $lien = $this->app_root;
        $html .=<<<EOT
       
            <article>
                <a class="list" href='${lien}/main.php/add-item/'>
                    <div class="test">
                        <h3 class="upperCase">Ajouter un nouveau Cadeau</h3>
                        <img class="basicImg" src="../../html/img/add.png" name="addList" />
                    </div>
                </a>
            </article>
       

        </div>

        
EOT;
 
        return ($html);
    }

    private function renderViewEditList(){
        $nom =$this->data->nom;
        $id=$this->data->id;
        $description = $this->data->description;
        $date_limite = $this->data->date_limite;
        $prenom_destinataire = $this->data->prenom_destinataire;
$html =<<< EOT
        <h2>Modifier une liste</h2>
        <form class="center formulaire" action="send/?id=${id}" method="post">
            <input type="text" name="nom" value="${nom}">
            <textarea type="text" name="description">${description}</textarea>
            <input type="text" name="date_limite" value="${date_limite}">
            <label>Pour moi<input type="checkbox" name="pour_moi" checked></label>
            <input type="text" name="prenom_destinataire" placeholder="Prénom du destinataire" name="${prenom_destinataire}">
            <input class="btn btnSucces" type="submit" value="Modifiez">
        </form>
EOT;
    return ($html);

}

private function renderAddListMessage(){
    $linkform = $this->script_name."/add-list-message/send/";
    
        $html =<<< EOT
        <h2>Ajouter un message pour la liste</h2>
        <form action="${linkform}" method="post">
            <input type="text" name="message" placeholder="Inserez votre message ici">
            <input type="submit" value="Envoyez">
        </form>
EOT;
    return $html;
}   

private function renderHome(){
    $linkform = $this->script_name."/verifyLogin/";
    $linkform2 = $this->script_name."/invit/";

    $html =<<<EOT
        <div>
            <form class="center formulaire" id="formLogin" action="${linkform}" method="POST">
                <h2>Connexion</h2>
                <input id='mail' type='email' placeholder='Mail' name='mail' required="required" maxlength="50">
                <input id='mdp' type='password' placeholder='Mot de passe' name='mdp' required="required" maxlength="25">
                <input class='btn btnSucces' type='submit' value='Connexion'>
            </form>
        </div>

EOT;
        return ($html);
    }

private function renderFooter(){

$html = <<<EOT
        <p></p>
EOT;

    return $html;
}

private function renderSignUp() {
    $html = <<<EOT
       <h2>Inscrivez-vous !</h2>
           <form class="formulaire" action= "../check_signUp/" method=post>
           <input type="text" name="nom" placeholder="Nom" required="required" maxlength="25">
           <input type="text" name="prenom" placeholder="Prénom" required="required" maxlength="25">
           <input type="email" name="mail" placeholder="Mail" required="required" maxlength="50">
           <input type="password" name="pass"placeholder="Mot de passe" required="required" maxlength="25">
           <input class="btn btnSucces" type="submit" value="Validez">
           </form>

EOT;

   
       return $html;

}

private function renderViewAddItem(){
        $liste_id = $_SESSION['liste_id'];
        $html =<<< EOT
        <h2>Ajouter un cadeau</h2>
        <form class="center formulaire" action="../add-itemTraitement/?liste_id=${liste_id}" method="post">
        <input type="text" name="nom" placeholder="Nom" required="required" maxlength="25">
        <textarea name="description" placeholder="Description" required="required" maxlength="255"></textarea>
        <input type="text" name="tarif" placeholder="Tarif" required="required" pattern="^[0-9]+(\.[0-9]{1,2})?$">
        <input type="text" name="url" placeholder="Lien du cadeau" maxlength="255">
                <input type="text" name="img" placeholder="Lien de l'image" maxlength="255">
        <label>Participe à une cagnotte</label><input type="checkbox" name="cagnotte">

        <select>
            <option value="">Fait partie d'aucun groupe</option>
            <option value="">ExGroupe</option>
        </select>

        <input class="btn btnSucces" type="submit" value="Valider">
        </form>

EOT;
    
    return $html;
    
}

private function renderViewEditItem(){
	$id=$this->data->id;
	$nom = $this->data->nom;
	$description = $this->data->description;
	$tarif = $this->data->tarif;
	$url = $this->data->url;
	$image = $this->data->image;
	$html =<<< EOT
        <h2>Modifer un item</h2>
        <form class="center formulaire" action="send/?id=${id}" method="post">
        <input type="text" name="nom" value ="${nom}">
        <textarea name="description">${description}</textarea>
        <input type="text" name="tarif" required="required" pattern="^[0-9]+(\.[0-9]{1,2})?$" value="${tarif}">
        <input type="text" name="url" value ="url">
        <input type="text" name="image" value="${image}">
        <label>Participe à une cagnotte<input type="checkbox" name="cagnotte"></label>
        <select>
            <option>Aucun Groupe</option>
            <option>unGroupe</option>
        <input type="submit" value="Validez">
        </form>
EOT;
    
    return $html;
    
}

private function renderViewAddMessageItem(){
        $html =<<< EOT
        <h2>Ajouter un message sur cet item</h2>
        <form action="#" method="post">
        <input type="text" name="nom" placeholder="Inserez votre nom">
        <input type="text" name="prenom" placeholder="Inserez votre prénom">
        <input type="text" name="message" placeholder="Inserez votre message">
        <input type="submit" value="Validez">
        </form>
EOT;
    
    return $html;
    
}

    private function renderViewLogin(){
        //$chaine = "<p>Bienvenue.</p>";
        //$chaine .= "<a href='".$this->script_name."'></a>";
        return $chaine;
    
    }

    private function renderAddList(){
     $html =<<<EOT
        <h2 class="center">Ajouter une liste</h2>
        <form action="send" method="post">
            <div class="center formulaire">
                
                    <input type="text" name="nom" placeholder="Nom" required="required" maxlength="25">
                    <textarea name="description" placeholder="Description" required="required" maxlength="255"></textarea>
                    <input type="text" name="date_limite" placeholder="Date limite de réservation" required="required" pattern="^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$">
                
                
                    <label>Pour sois-même<input type="checkbox" name="soisMeme" checked></label>
                    <input type="text" name="prenomDestinataire" placeholder="Prénom du destinataire">
          <input class="btn btnSucces" type="submit" value="Sauvegarder la liste">
                </section>
            </div>
        </from>
EOT;
    return $html;
 }

 private function redirect(){
    $lien = $this->app_root;
    header("Location: $lien/main.php/list/");
}

private function redirect2(){
    $lien = $this->app_root;
    header("Location: $lien/main.php/list-items/");
}

protected function renderBody($selector=null){
    $my_head = $this->renderHeader();
    $my_foot = $this->renderFooter();

    switch($selector){
        case 'home':{$main=$this->renderHome();break;};
        case 'login':{$main=$this->renderViewLogin();break;};
        case 'liste':{$main=$this->renderViewListe();break;};
        case 'addListMessage':{$main=$this->renderAddListMessage();break;};
        case 'list_items':{$main=$this->renderViewListItem();break;};
        case 'list_items_url':{$main=$this->renderViewListItemUrl();break;};
        case 'edit-item': {$main=$this->renderViewEditItem();break;}
        case 'edit-list':{$main=$this->renderViewEditList();break;};
        case 'add-list':{$main=$this->renderAddList();break;};
        case 'signUp':{$main=$this->renderSignUp();break;};
        case 'redirect':{$main=$this->redirect();break;};
        case 'redirect2':{$main=$this->redirect2();break;};
        case 'addItem':{$main=$this->renderViewAddItem();break;}
        case 'show-reserve-form':{$main=$this->showReserveForm();break;}
        case 'redirect3':{$main=$this->renderViewListItemUrl();break;}

        default:{$main=$this->renderHome();break;};
    }

        $html = <<<EOT
<header class='header'>
    ${my_head}
</header>

<section>

    ${main}

</section>

<footer class="footer">
  ${my_foot}
</footer>

EOT;

        return  $html;
        
    }

    private function renderViewListItemUrl(){
        $lien = $this->app_root;

        $html =<<<EOT
        <h2>Liste : </h2>
        <div class="grilleListe">
EOT;

foreach ($this->data as $v) {
    $nom = $v->nom;
    $image = $v->image;
    $desc = $v->description;
    $date_limite = $v->date_limite;
    $url = $v->url;
    $tarif=$v->tarif;
    $id = $v->id;
    $participate = '';

    $reserver='';
    $acheteur=\mecadoapp\model\Acheteur::where('id_item', '=', $id)->first();
    if(!count($acheteur)>=1){
        if(intval($v->cagnotte) == 0){
            $reserver = '';}else{$reserver = "<a class='btn btnNotif list' href='${lien}/main.php/show-reserve-form/?item_id=${id}'>RESERVER</a>";
        }
    }
       if ($v->image ==null) {
        $image = "$lien/html/img/default.png";
       }
       else {

           $image = $v->image;
       }
    
    if($v->cagnotte==0){$participate='';}else{$participate="<a class='btn btnWarning list' href='#'>PARTICIPER</a>";}
        $html .=<<<EOT
          <article class="item">
                <a class="list" href="">
                    <div>
                        <h3 class="upperCase">${nom}</h3>
                        <div class="divImg">
                        <img class="basicImg" src="${image}" name="image">
                        </div>
                        <p class="bold">${desc}</p>
                        <p>${tarif} €</p>
                        
                    </div>
                </a>
                <a href="${url}" class="bold">URL : ${url}</a>

                <div class="btnInvit">
            ${reserver}
            ${participate}

                </div>

            </article>
        
    
        
EOT;
        }
        return ($html);
    }

//===================================================
// SHOW RESERVE FORM
//===================================================

private function showReserveForm(){
        $id=$this->data->id;
        $nom = $this->data->nom;
        $description = $this->data->description;
        $tarif = $this->data->tarif;
        $html =<<< EOT
        <h2>RESERVER ${nom}</h2>
        <form class="center formulaire" action="../reserve-item/?item_id=${id}" method="post">
            <p>${description}</p>
            <p>${tarif} €</p>
            <input type="text" name="nom" placeholder="Nom" required="required" maxlength="25">
            <input type="text" name="prenom" placeholder="Prénom" required="required" maxlength="25">
            <textarea type="text" name="message" placeholder="Message" maxlength="255" required="required"></textarea>   
            <input class="btn btnSucces" type="submit" value="RESERVER">
        </form>
EOT;
    
    return $html;
    
}


}
