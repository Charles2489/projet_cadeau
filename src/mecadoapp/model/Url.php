<?php

namespace mecadoapp\model;

class Url extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'url';
	protected $primarykey = 'id';
	public $timestamps = false;

    public function list_(){
		return $this->belongsTo('\mecadoapp\model\List_', 'id_liste');
	}
}
