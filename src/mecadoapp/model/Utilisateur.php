<?php

namespace mecadoapp\model;

class Utilisateur extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'utilisateur';
	protected $primarykey = 'id';
	public $timestamps = false;

	public function lists(){

		return $this->hasMany('\mecadoapp\model\List','id');
	}
}