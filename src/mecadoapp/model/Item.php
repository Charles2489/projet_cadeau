<?php

namespace mecadoapp\model;

class Item extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'item';
	protected $primarykey = 'id';
	public $timestamps = false;

    public function items(){

		return $this->hasMany(\mecadoapp\model\Item::class,'id');
	}


	public function acheteur() {
	       return $this->belongsTo('\tweeterapp\model\Acheteur', 'id_item');
	}
}
