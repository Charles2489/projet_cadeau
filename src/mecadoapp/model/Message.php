<?php

namespace mecadoapp\model;

class Message extends \Illuminate\Database\Eloquent\Model {

	protected $table = 'message';
	protected $primarykey = 'id';
	public $timestamps = false;

	public function list_() {
	       return $this->belongsTo('\tweeterapp\model\List_', 'id_liste');
	}

}
