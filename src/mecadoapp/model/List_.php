<?php

namespace mecadoapp\model;

class List_ extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'liste';
	protected $primarykey = 'id';
	public $timestamps = false;

    public function utilisateur(){

		return $this->belongsTo(\mecadoapp\model\Utilisateur,'id_utilisateur');
	}

	public function items() {
	       return $this->hasMany('\mecadoapp\model\Item', 'id_liste');
	}

	public function url() {
	       return $this->hasOne('\mecadoapp\model\Url', 'id_liste');
	}

	public function messages() {
		return $this->hasMany('\mecadoapp\model\List_' , 'id_liste');
	}

}
