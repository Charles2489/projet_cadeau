<?php

namespace mecadoapp\model;

class Acheteur extends \Illuminate\Database\Eloquent\Model{

	protected $table = 'acheteur';
	protected $primarykey = 'id';
	public $timestamps = false;

	public function items() {
	       return $this->hasMany('\tweeterapp\model\Item', 'id_item');
	}
}
