<?php

namespace mecadoapp\auth;

class MecadoAuthentification extends \mf\auth\Authentification{



    const ACCESS_LEVEL_USER  = 100;   
    const ACCESS_LEVEL_ADMIN = 200;

	public function __construct(){
        parent::__construct();
    }


	

	// public function createUser($username, $pass, $fullname,$level=self::ACCESS_LEVEL_USER){
	// 	$user = User::where('username', '=', $username);

	// 	if(isset($user->username)){
	// 		throw new \mf\auth\exception\AuthentificationException('Utilisateur existe déjà.');
	// 	}
	// 	else{
	// 		$user = User::table('user')->insert([
	// 			'fullname' => $fullname,
	// 			'username' => $username,
	// 			'password' => $pass,
	// 			'level' => $level
	// 		]);
	// 	}
	// }

    /* La méthode login
     *  
     * permet de connecter un utilisateur qui a fourni son nom d'utilisateur 
     * et son mot de passe (depuis un formulaire de connexion)
     *
     * @param : $username : le nom d'utilisateur   
     * @param : $password : le mot de passe tapé sur le formulaire
     *
     * Algorithme :
     * 
     *  Récupérer l'utilisateur avec le nom d'utilisateur $username depuis la BD
     *  Si aucun de trouvé 
     *      soulever une exception 
     *  sinon 
     *      si $password correspond au mot de passe crypté en BD 
     *          charger la session de l'utilisateur
     *      sinon soulever une exception
     *
     */
    
    public function login($username, $mdp){
        $mail = $_POST['mail'];
        $mdp = $_POST['mdp'];
        $user = \mecadoapp\model\Utilisateur::where('mail', '=', $mail)->first();
        //echo $user->mdp;
        //var_dump($user);
        //echo $user->mail;
        if(!isset($user)){
            throw new \mf\auth\exception\AuthentificationException('Vous devez renseigner une adresse mail.');
        }
        else{
            if(\mf\auth\Authentification::verifyPassword($mdp, $user->mdp)){
            	$this->updateSession($user->prenom, $user->level);
                var_dump($user);
                return $user->id;
            }
            else{
                throw new \mf\auth\exception\AuthentificationException('Mdp incorrect !');
            }
        }

    }

}