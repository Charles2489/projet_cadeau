<?php

namespace mecadoapp\control;

use \mecadoapp\auth\MecadoAuthentification as Auth;

class MecadoController extends \mf\control\AbstractController {

	public function __construct(){
		parent::__construct();
	}


//=======================================================
// AUTH / CONNEXION / HOME
//=======================================================

	public function viewHome(){
		$vue = new \mecadoapp\view\MecadoView(null);
		return $vue->render('home');
	}

	public function viewSignUp(){
		$vue = new \mecadoapp\view\MecadoView(null);
		return $vue->render('signUp');
	}

	public function viewVerifyLogin(){

		if(isset($this->request->post['mail'])){
			$form=$this->request->post;
		}

		if(isset($form)){
			$user = new \mecadoapp\auth\MecadoAuthentification();
			$user->login($form['mail'], $form['mdp']);
			$id = (  $user->login($form['mail'], $form['mdp']));
			$requete = \mecadoapp\model\List_::select();
			$_SESSION["user_id"] =$id;
			$lignes = $requete->where('id_utilisateur', '=', $id)->get();
			$vue = new \mecadoapp\view\MecadoView($lignes);
			return $vue->render('redirect');
		}
		else{
			$vue = new \mecadoapp\view\MecadoView(NULL);
			return $vue->render('home');
		}
	}

	public function viewCheckSignUp(){
		$user = new \mecadoapp\model\Utilisateur();
		$test= \mecadoapp\model\Utilisateur::where('mail', '=', $_POST['mail'])->first();
		echo $test;        
		if (isset($test->mail)) {
			throw new \mf\auth\exception\AuthentificationException('Utilisateur existe déjà.');
		}
		else {
			$user->nom = $_POST['nom'];
			$user->prenom = $_POST['prenom'];
			$user->mail = $_POST['mail'];
			$user->mdp = $_POST['pass'];
			$user->save();            
			$vue = new \mecadoapp\view\MecadoView(null);
			return $vue->render('home');
		}        
	}

	public function logout(){
		header('Location: ../../main.php');
		unset($_SESSION['user_login']);
		unset($_SESSION['access_level']);
		unset($_SESSION['user_id']);
	}


//=======================================================
// SHOW RESERVE FORM
//=======================================================

	public function showReserveForm(){
		$item=\mecadoapp\model\Item::where('id', '=', $_GET["item_id"])->first();
		$v = new \mecadoapp\view\MecadoView($item);
		$v->render('show-reserve-form');
	}

//=======================================================
// LIST
//=======================================================

	public function viewListItems(){
		if (isset($_GET['liste_id'])) {
			$_SESSION['liste_id'] = $_GET['liste_id'];
		}    
		$list = \mecadoapp\model\List_::where('id', '=', $_SESSION["liste_id"])->first();  
		$liste_items = $list->items()->where('item_parent', '=', NULL)->get();
		$liste_items = $list->items()->get();

		$message = \mecadoapp\model\Message::where('id_liste', '=', $_SESSION["liste_id"])->get();

		$v = new \mecadoapp\view\MecadoView($liste_items);
		$v->render('list_items'); 
	}

	public function viewList() {
		if (empty ($_GET["id"])) {
			$id= $_SESSION["user_id"];
		}
		else  {
			$id = $_GET["id"];
		}

		$requete = \mecadoapp\model\List_::select();
		$lignes = $requete->where('id_utilisateur', '=', $id)->get();
		$vue = new \mecadoapp\view\MecadoView($lignes);
		return $vue->render('liste');
	}


	public function addListMessage(){
		$_SESSION['liste_id'] = $_GET['liste_id'];
		$v = new \mecadoapp\view\MecadoView(null); 
		$v->render('addListMessage');
	}

	public function addListMessageSend(){


		$list=\mecadoapp\model\Utilisateur::where('id', '=', $_SESSION['user_id'])->first();
		if(count($list)>=1){

			$message = new \mecadoapp\model\Message();

			$message ->message = $_POST['message'];
			$message ->auteur = $_SESSION['user_id'];
			$message ->id_liste = $_SESSION['liste_id'];

			$message->save();
		}
		else  {echo "el item no existe";}

		$v = new \mecadoapp\view\MecadoView(null); 
		$v->render('redirect2');


	}

	public function viewAddList () {
		$v = new \mecadoapp\view\MecadoView(NULL);
		$v->render('add-list'); 
	}
	public function addList(){

		$list = new \mecadoapp\model\List_();

		$list ->nom = $_POST['nom'];
		$list ->description = $_POST['description'];
		$list ->date_limite = $_POST['date_limite'];

		$list ->prenomDestinataire = $_POST['prenomDestinataire'];
		$list ->id_utilisateur = $_SESSION['user_id'];

		$list->save();
		$vue = new \mecadoapp\view\MecadoView(NULL);
		return $vue->render('redirect');

	}
	public function viewEditList() {
		$list= \mecadoapp\model\List_::select()->where('id', '=', $_GET["id"])->where('id_utilisateur','=',$_SESSION["user_id"])->first();
		$vue = new \mecadoapp\view\MecadoView($list);
		return $vue->render('edit-list');
	}

	public function editList(){

		$list=\mecadoapp\model\List_::where('id', '=', $_GET["id"])->where('id_utilisateur','=',$_SESSION["user_id"])->first();
		if(count($list)>=1){
			$list ->nom = $_POST['nom'];
			$list ->description = $_POST['description'];
			$list ->date_limite = $_POST['date_limite'];

			$list ->prenomDestinataire = $_POST['prenomDestinataire'];
			$list->save();
			$vue = new \mecadoapp\view\MecadoView(NULL);
			return $vue->render('redirect');
		}
	}

	public function deleteList(){
		$list=\mecadoapp\model\List_::select()->where('id', '=', $_GET["id"])->where('id_utilisateur','=',$_SESSION["user_id"])->first();
		if(count($list)>=1){
			$list->delete();
			$vue = new \mecadoapp\view\MecadoView(NULL);
			return $vue->render('redirect');
		}
	}

	public function detailList(){

		$list=\mecadoapp\model\List_::where('id', '=', $_GET["liste_id"])->first();

	}


//=======================================================
// VIEW LIST URL
//=======================================================

	public function viewListUrl(){
		$url=\mecadoapp\model\Url::where('id_liste', '=', $_GET["liste_id"])->first();

	}

//=======================================================
// VIEW URL LIST
//=======================================================

	public function viewUrlList(){
		$url=\mecadoapp\model\Url::where('liste_url', '=', $_GET["liste_url"])->first();
		$list=\mecadoapp\model\List_::where('id', '=', $url->id_liste)->first();
		$liste_items = $list->items()->where('item_parent', '=', NULL)->get();

		$v = new \mecadoapp\view\MecadoView($liste_items);
		$v->render('list_items_url');

	}

//=======================================================
// ITEM
//=======================================================

	public function viewItem() {
		$id = $_GET['id'];
$requete = \mecadoapp\model\Item::select();  // SQL : select * from 'tweet'
$lignes = $requete->where('id', '=', $id)->first();

}



public function addItemTraitement(){
	$list = \mecadoapp\model\List_::where('id', '=', $_SESSION["liste_id"])->first();
	if(count($list)>=1){

		$item = new \mecadoapp\model\Item();

		$item->nom = $_POST['nom'];
		$item->description = $_POST['description'];
		$item->tarif = $_POST['tarif'];
		$item->url = $_POST['url'];
		$item->image = $_POST['img'];
		$item->groupe = 0;
		$item->cagnotte = $_POST['cagnotte'];
		$item->item_parent = NULL;
		$item->id_liste = $_SESSION["liste_id"];


		$item ->save();
	}
	$v = new \mecadoapp\view\MecadoView(null); 
	$v->render('redirect2');

}


public function viewAddItem(){
	$v = new \mecadoapp\view\MecadoView(null); 
	$v->render('addItem');

}
public function viewEditItem (){
	$item= \mecadoapp\model\Item::select()->where('id', '=', $_GET["id"])->first();
	$vue = new \mecadoapp\view\MecadoView($item);
	return $vue->render('edit-item');
}

public function deleteItem(){

	$item=\mecadoapp\model\Item::select()->where('id', '=', $_GET["id"])->first();
	if(count($item)>=1){
		$item->delete();
	}
	$vue = new \mecadoapp\view\MecadoView(NULL);
	return $vue->render('redirect2');


}

public function reserveItem(){

	$item=\mecadoapp\model\Item::where('id', '=', $_GET["item_id"])->first();
	if(count($item)>=1){
		$acheteur = new \mecadoapp\model\Acheteur();

		$acheteur ->nom = $_POST['nom'];
		$acheteur ->prenom = $_POST['prenom'];
		$acheteur ->participation = NULL;
		$acheteur ->message = $_POST['message'];
		$acheteur ->id_item = $_GET["item_id"];

		$acheteur ->save();
	}
	$list=\mecadoapp\model\List_::where('id', '=', $_SESSION['liste_id'])->first();
	$liste_items = $list->items()->where('item_parent', '=', NULL)->get();
	$v = new \mecadoapp\view\MecadoView($liste_items); /* Contient une liste de Tweets*/
	$v->render('list_items_url'); /* Le sélecteur est la chaîne home */
}

public function participateItem(){

	$item=\mecadoapp\model\Item::where('id', '=', $_GET["item_id"])->first();
	if(count($item)>=1){
		if($item->cagnotte==1){

			$acheteur = new \mecadoapp\model\Acheteur();


			$acheteur ->nom = $_POST['nom'];
			$acheteur ->prenom = $_POST['prenom'];
			$acheteur ->participation = $_POST['participation'];
			$acheteur ->message = $_POST['message'];
			$acheteur ->id_item = $_GET["item_id"];
			$acheteur ->save();
		}
	}



}


public function addSubItem(){
	$item=\mecadoapp\model\Item::where('id', '=', $_GET["item_id"])->first();
	if(count($item)>=1 and $item->item_parent=NULL){

		$newItem = new \mecadoapp\model\Item();

		$newItem ->nom = $_POST['nom'];
		$newItem ->description = $_POST['description'];
		$newItem ->tarif = $_POST['tarif'];
		$newItem ->url = $_POST['url'];
		$newItem ->image = $_POST['image'];
		$newItem ->groupe = '0';
		$newItem ->cagnotte = $_POST['cagnotte'];
		$newItem ->item_parent = $_GET["item_id"];
		$newItem ->id_liste = $item->id_liste;

		$newItem ->save();
	}

}


public function editItem() {
	$item=\mecadoapp\model\Item::where('id', '=', $_GET["id"])->first();
	if(count($item)>=1){
		$item ->nom = $_POST['nom'];
		$item ->description = $_POST['description'];
		$item ->image = $_POST['image'];
		$item ->url = $_POST['url'];
		$item ->tarif = $_POST['tarif'];
// $list ->pour_moi = $_POST['pour_moi'];
		$item->save();
		$vue = new \mecadoapp\view\MecadoView(NULL);
		$vue->render('redirect2');
	}
}

}