<?php

session_start();

use mf\router\Router as router;
/* pour le chargement automatique des classes dans vendor */
require_once 'vendor/autoload.php';

require_once 'src/mf/utils/ClassLoader.php';
$loader = new ClassLoader('src');
$loader->register();

$get_conf = parse_ini_file("conf/config.ini");
$config = [
	'driver'    => $get_conf["driver"],
	'host'      => $get_conf["host"],
	'database'  => $get_conf["database"],
	'username'  => $get_conf["username"],
	'password'  => $get_conf["password"],
	'charset'   => $get_conf["charset"],
	'collation' => $get_conf["collation"],
	'prefix'    => '' 
];
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config );
$db->setAsGlobal();
$db->bootEloquent();


$router = new router();

$router->addRoute('home',    '/home/',         '\mecadoapp\control\MecadoController', 'viewHome');

//ROUTE POUR LA PARTIE CONNECTION ET INSCRIPTION
$router->addRoute('verifyLogin','/verifyLogin/', '\mecadoapp\control\MecadoController', 'viewVerifyLogin');
$router->addRoute('login','/login/', '\mecadoapp\control\MecadoController', 'viewLogin');

//ROUTE DE DECONNEXION
$router->addRoute('deconnexion','/deconnexion/', '\mecadoapp\control\MecadoController', 'deconnexion');


$router->addRoute('list',    '/list/',         '\mecadoapp\control\MecadoController', 'viewList');
$router->addRoute('add-list', '/add-list/',         '\mecadoapp\control\MecadoController', 'viewAddList');
$router->addRoute('add-send',    '/add-list/send',         '\mecadoapp\control\MecadoController', 'AddList');
$router->addRoute('edit-send',    '/edit-list/send/',         '\mecadoapp\control\MecadoController', 'editList');
$router->addRoute('edit-list',    '/edit-list/',         '\mecadoapp\control\MecadoController', 'viewEditList');
$router->addRoute('delete-list',    '/delete-list/',         '\mecadoapp\control\MecadoController', 'deleteList');
$router->addRoute('detail-list',    '/detail-list/',         '\mecadoapp\control\MecadoController', 'detailList');
$router->addRoute('view-list-url',    '/view-list-url/',         '\mecadoapp\control\MecadoController', 'viewListUrl');
$router->addRoute('view-url-list',    '/view-url-list/',         '\mecadoapp\control\MecadoController', 'viewUrlList');

$router->addRoute('check_login',    '/check_login/',         '\mecadoapp\control\MecadoController', 'viewCheckLogin');

$router->addRoute('list-items',    '/list-items/',         '\mecadoapp\control\MecadoController', 'viewListItems');
$router->addRoute('add-item',    '/add-item/',         '\mecadoapp\control\MecadoController', 'viewAddItem');
$router->addRoute('add-itemTraitement',    '/add-itemTraitement/',      '\mecadoapp\control\MecadoController', 'addItemTraitement');


$router->addRoute('edit-item',    '/edit-item/',         '\mecadoapp\control\MecadoController', 'viewEditItem');
$router->addRoute('edit-send',    '/edit-item/send/',         '\mecadoapp\control\MecadoController', 'editItem');
$router->addRoute('delete-item',    '/delete-item/',         '\mecadoapp\control\MecadoController', 'deleteItem');
$router->addRoute('add-sub-item',    '/add-sub-item/',         '\mecadoapp\control\MecadoController', 'addSubItem');

$router->addRoute('reserve-item',    '/reserve-item/',         '\mecadoapp\control\MecadoController', 'reserveItem');
$router->addRoute('show-reserve-form',    '/show-reserve-form/',         '\mecadoapp\control\MecadoController', 'showReserveForm');
$router->addRoute('participate-item',    '/participate-item/',         '\mecadoapp\control\MecadoController', 'participateItem');
$router->addRoute('add-list-message',    '/add-list-message/',         '\mecadoapp\control\MecadoController', 'addListMessage');
$router->addRoute('add-list-message',    '/add-list-message/send/',     '\mecadoapp\control\MecadoController', 'addListMessageSend');

$router->addRoute('login',    '/auth/',         '\mecadoapp\control\MecadoController', 'viewLogin');
$router->addRoute('check_login',    '/check_login/',         '\mecadoapp\control\MecadoController', 'viewCheckLogin');
$router->addRoute('signUp',    '/signUp/',         '\mecadoapp\control\MecadoController', 'viewSignUp');
$router->addRoute('checkSignUp', '/check_signUp/', '\mecadoapp\control\MecadoController', 'viewCheckSignUp');

$router->addRoute('default', 'DEFAULT_ROUTE',  '\mecadoapp\control\MecadoController', 'viewHome');
$router->addRoute('logout', '/logout/', '\mecadoapp\control\MecadoController', 'logout');


$router->run();
